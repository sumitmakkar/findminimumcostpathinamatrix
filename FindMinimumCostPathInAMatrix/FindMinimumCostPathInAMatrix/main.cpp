#include<iostream>
#include<vector>

using namespace std;

class Engine
{
    private:
	   vector< vector<int> > matrixVector;
	   vector< vector<int> > pathVector;
    
	   void initializePathVector(int r , int c)
	   {
           for(int i = 0 ; i < r ; i++)
           {
               vector<int> innerVector;
               for(int j = 0 ; j < c ; j++)
               {
                   innerVector.push_back(0);
               }
               pathVector.push_back(innerVector);
           }
       }
    
	   void displayMatrix(vector< vector<int> > matVector , int r , int c)
	   {
           for(int i = 0 ; i < r ; i++)
           {
               for(int j = 0 ; j < c ; j++)
               {
                   cout<<matVector[i][j]<<" ";
               }
               cout<<endl;
           }
       }
    
    public:
	   Engine(vector< vector<int> > mV , int rS , int cS)
	   {
           matrixVector = mV;
           initializePathVector(rS , cS);
       }
    
	   int findMinimumCost(int r , int c)
	   {
           for(int i = 0 ; i < r ; i++)
           {
               for(int j = 0 ; j < c ; j++)
               {
                   if(!i)
                   {
                       if(!j)
                       {
                           pathVector[i][j] = matrixVector[i][j];
                       }
                       else
                       {
                           pathVector[i][j] = matrixVector[i][j] + pathVector[i][j-1];
                       }
                   }
                   else if(!j)
                   {
                       pathVector[i][j] = pathVector[i-1][j] + matrixVector[i][j];
                   }
                   else
                   {
                       int minLen       = min(pathVector[i-1][j] , pathVector[i][j-1]);
                       minLen           = min(pathVector[i-1][j-1] , minLen);
                       pathVector[i][j] = matrixVector[i][j] + minLen;
                   }
               }
           }
           //displayMatrix(pathVector , r , c);
           return pathVector[r-1][c-1];
       }
    
        void printPathInMatrix(int r , int c)
        {
            vector<int> pathArray;
            pathArray.push_back(matrixVector[r][c]);
            while (r || c)
            {
                if(r && c)
                {
                    if(pathVector[r][c-1] < pathVector[r-1][c-1] && pathVector[r][c-1] < pathVector[r-1][c])
                    {
                        pathArray.push_back(matrixVector[r][c-1]);
                        c--;
                    }
                    else if(pathVector[r-1][c-1] < pathVector[r][c-1] && pathVector[r-1][c-1] < pathVector[r-1][c])
                    {
                        pathArray.push_back(matrixVector[r-1][c-1]);
                        r--;
                        c--;
                    }
                    else
                    {
                        pathArray.push_back(matrixVector[r-1][c]);
                        r--;
                    }
                }
                else if(!c && r)
                {
                    pathArray.push_back(matrixVector[r-1][c]);
                    r--;
                }
                else
                {
                    pathArray.push_back(matrixVector[r][c-1]);
                    c--;
                }
            }
            pathArray.push_back(matrixVector[0][0]);
            int len = (int)pathArray.size();
            while (--len)
            {
                cout<<pathArray[len-1]<<" ";
            }
            cout<<endl;
        }
};

int main()
{
//    const int rowSize = 4;
//    const int colSize = 3;
//    int arr[rowSize][colSize] = {3,2,8,1,9,7,0,5,2,6,4,3};
    const int rowSize = 3;
    const int colSize = 4;
    int arr[rowSize][colSize] = {1,3,5,8,4,2,1,7,4,3,2,3};
    vector< vector<int> > matrixVector;
    for(int i = 0 ; i < rowSize ; i++)
    {
        vector<int> innerMatrix;
        for(int j = 0 ; j < colSize ; j++)
        {
            innerMatrix.push_back(arr[i][j]);
        }
        matrixVector.push_back(innerMatrix);
    }
    Engine e = Engine(matrixVector , rowSize , colSize);
    cout<<e.findMinimumCost(rowSize , colSize)<<endl;
    e.printPathInMatrix(rowSize - 1 , colSize - 1);
    return 0;
}